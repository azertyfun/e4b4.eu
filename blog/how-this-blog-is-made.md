* date: 2021-06-03
* title: How this blog is made
* description: Let's kick things off with what this thing even is

Introduction
------------

Writing a blog is kind of a cliché "first project of your third year of a very boring school program" kinda deal, but I figured it might be interesting anyway.

I'm not much of a front-end guy (you might have been tipped of by the spartan design of this website). More than that, I am ideologically opposed to website that require javascript to run for no discernable reason. Or to the use of javascript in the backend.

... I might have a slight dislike for javascript in general. *ahem*.

Regardless, for performance, ideological, and practical reasons, this website is entirely statically generated in the laziest way possible.

Overview
--------

A ≈ 50 LOC python script uses jinja2 to fit HTML content into a template, and also converts a bunch of markdown file to HTML with [python-markdown](https://python-markdown.github.io) which is also templated using jinja2.

Simple as that! Generation happens in less than 100 ms, and I'm left with a bunch of HTML files with no server-side requirements whatsoever. No need for a rest API if the content of the page changes once a month…

Implementation
--------------

The trick is that jinja2 is made for just this kind of stuff. Even though most people only know of it because of its use as the templating language for [ansible](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#referencing-simple-variables), it's capable of much more.

### The jinja

I simply use a base `template.html` file for setting the base layout, that can be [inherited](https://jinja.palletsprojects.com/en/3.0.x/templates/#template-inheritance) by any actual page.

#### `template.html`

```html
<!DOCTYPE HTML>
<html>
    <!-- … -->

    <body>
        <!-- … -->
        {% block content %}{% endblock %}
        <!-- … -->
    </body>
</html>
```

#### `index.html.j2`

```html
{% extends "template.html" %}

{% block content %}
<main>
    <h1>Welcome!</h1>

    <!-- … -->
</main>
{% endblock %}
```

### Generating the files

The following is enough to generate a template file:

```python
loader = jinja2.FileSystemLoader(Path('src/')) # include-path equivalent
env = jinja2.Environment(loader=loader)
tpl = env.get_template('index.html.j2') # load the template
with open('out.html') as fp:
    fp.write(tpl.render(
        title='Home'
    ))
```

I've just done the same in a for-loop iterating over static pages (such as `blog.html`) and the markdown files.

### Conclusion

Not that complicated is it? I just love that I can update the blog by running two commands (`./gen.py && scp dist/* my-server:/var/www`). Guess I'll need to set up a CI at some point.

You can see the whole repository [here](https://gitlab.com/azertyfun/e4b4.eu).
