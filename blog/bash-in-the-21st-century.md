* date: 2021-06-04
* title: Bash in the 21st century
* description: Your shell needn't be a featureless black hole

It seems that the people I've interacted with can be separated into two groups: those using `zsh` with all kinds of
bells and whistles, and those using `bash` with barely more functionality than a PDP-11 might provide.

While I tried zsh and it is really nice, I like the universality of bash and that everything I learn about it is
usable on just about any GNU environment out there.

But because pretty much all mainstream distros don't enable the good features™, many people are unaware of how far
bash has come in the past decades. Let's go through my bashrc...

## Shell options

The only reason these aren't defaults is backwards compatibility. No reason to leave them disabled!

```bash
set -o notify # Show a message in the console when a background job finishes
shopt -s cdspell # automatically correct typos when `cd`ing into a directory
shopt -s checkhash # check that commands in the hash table exist before executing them
shopt -s cmdhist # save multiline commands
shopt -s histappend # do not overwrite bash_history to allow for multiple shells open at once (!)
shopt -s histreedit histverify # improve history substitutions
shopt -s extglob # improved globbing (e.g. `**/*.png`)
bind 'set enable-bracketed-paste on' # do not execute a copy-pasted command if it contains a newline
```

Note: `bracketed-paste` will *not* protect you from malicious websites hijacking your clipboard, as they can just insert the escape code as well to force the command to be executed. It will save you from yourself though.

You can find a bunch more in the `shopt` question of `man bash`.

## `vi` editing mode

Nothing pains me more than watching a collegue painfully hold the left arrow down for several seconds to get to the
middle of a long command. Think of all the microsconds that could add up to *minutes* over an entire year!

You could either learn emacs text editing (ew), or switching to vi editing. The command prompt starts in insert mode
and you can switch to normal mode at any point by pressing escape.

```bash
bind 'set editing-mode vi'
```

I just can't live without it anymore.

[![xkcd 1205](https://imgs.xkcd.com/comics/is_it_worth_the_time.png "xkcd 1205")](https://xkcd.com/1205/)

## git status in PS1

```
~/src/e4b4.eu [master %=] $
```

This is not ZSH wizardry. It's actually the little known (to me at least) [official bash PS1 plugin](https://github.com/git/git/blob/master/contrib/completion/git-prompt.sh). If you have git installed this should enable it automatically:

```bash
if [ -e /usr/share/git/git-prompt.sh ]; then
    export GIT_PS1_SHOWDIRTYSTATE=1
    export GIT_PS1_SHOWUNTRACKEDFILES=1
    export GIT_PS1_SHOWSTASHSTATE=1
    export GIT_PS1_SHOWUPSTREAM=auto
    source /usr/share/git/git-prompt.sh
fi
```

You can then add `$(__git_ps1 " [%s]")` to your `$PS1`.

## Colorful man pages

```bash
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'
```

Just try it, it makes man pages full of color and *so much more readable it's not even funny*. Why `man` doesn't just read termcaps and enable this by itself, I don't know.

## `fzf`

I set my history size to a million so I can always go back to weird `jq` scripts I wrote months ago. Why not? After over two years it's barely hitting 2 MiB in size.

Now running `less ~/.bash_history` all the time is tiresome, and `fzf` is an awesome interactive interface that pops up when you press `ctrl+R` to fuzzy filter through your history. Truly a life saver, my colleagues think I'm a wizard when I am able to immediately perform a weird one-liner I wrote 6 months ago to parse some dubiously documented API.

## ... And more!

That's most of the useful stuff in *my* bashrc, but take the time to google stuff you can do to make your life easier.
