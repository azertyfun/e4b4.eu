#!/usr/bin/env python3

import sys
from pathlib import Path

import jinja2
import markdown

def include_raw(p):
    with open(p) as fp:
        return fp.read()

def main():
    # Build blog entries

    output_dir = Path(sys.argv[1])

    blog_entries = []
    for f in Path('blog/').glob('*.md'):
        with f.open() as fp:
            entry = {}
            text = ''
            in_meta = True
            for l in fp.readlines():
                if l[:2] != '* ':
                    in_meta = False

                if in_meta:
                    (k, v) = l[2:].strip().split(': ', 1)
                    entry[k.strip()] = v.strip()
                else:
                    text += l
            if not all(x in entry for x in ['date', 'title', 'description']):
                raise RuntimeError('Missing meta tags.')

            entry['text'] = text
            entry['stub'] = f.stem

        blog_entries.append(entry)

    blog_entries = list(reversed(sorted(blog_entries, key=lambda e: e['date'])))

    loader = jinja2.FileSystemLoader(Path('src/'))
    env = jinja2.Environment(loader=loader)
    for f in Path('src/pages/').glob('*.html'):
        tpl = env.get_template(f.relative_to('src/').as_posix())
        out = output_dir / f.name
        with out.open('w') as fp:
            fp.write(tpl.render(
                blog_entries=blog_entries,
                include_raw=include_raw,
                output_dir=output_dir,
            ))

    for entry in blog_entries:
        f = output_dir / f'blog-{entry["stub"]}.html'
        tpl = env.get_template('blog-entry.html')
        with f.open('w') as fp:
            fp.write(tpl.render(
                include_raw=include_raw,
                output_dir=output_dir,
                title=entry['title'],
                date=entry['date'],
                content=markdown.markdown(
                    entry['text'],
                    extensions=['fenced_code', 'codehilite'],
                    extension_configs={
                        'codehilite': {
                            'use_pygments': True,
                        }
                    }
                ),
            ))

if __name__ == '__main__':
    main()
