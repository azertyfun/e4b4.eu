.PHONY: all

WEB_SERVER=athena
DIST=dist

all: clean $(DIST) $(DIST)/style.css $(DIST)/pygments.css $(DIST)/favicon.png $(DIST)/nathan-monfils.jpg
	./gen.py $(DIST)

$(DIST):
	mkdir -p $@

$(DIST)/pygments.css:
	pygmentize -S native -f html -a .codehilite | sed -e 's|/\*.*\*/||g' -e 's| *$$||g' > $@

$(DIST)/%.css:
	sass -s compressed src/scss/$*.scss $@

$(DIST)/%:
	cp src/$* $@

deploy:
	ssh $(WEB_SERVER) sudo mkdir -p /var/www/e4b4.eu
	rsync --rsync-path="sudo rsync" src/nginx.conf $(WEB_SERVER):/etc/nginx/sites-enabled/e4b4.eu.conf
	rsync --rsync-path="sudo rsync" $(DIST)/* $(WEB_SERVER):/var/www/e4b4.eu
	ssh $(WEB_SERVER) sudo systemctl reload nginx

clean:
	rm -rf $(DIST)
